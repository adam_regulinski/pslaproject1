import numpy as np
import math


def normalize(input_matrix):
    """
    Normalizes the rows of a 2d input_matrix so they sum to 1
    """

    row_sums = input_matrix.sum(axis=1)
    assert (np.count_nonzero(row_sums)==np.shape(row_sums)[0]) # no row should sum to zero
    new_matrix = input_matrix / row_sums[:, np.newaxis]
    return new_matrix

    
class Corpus(object):

    """
    A collection of documents.
    """

    def __init__(self, documents_path):
        """
        Initialize empty document list.
        """
        self.documents = []
        self.vocabulary = []
        self.likelihoods = []
        self.documents_path = documents_path
        self.term_doc_matrix = None 
        self.document_topic_prob = None  # P(z | d)
        self.topic_word_prob = None  # P(w | z)
        self.topic_prob = None  # P(z | d, w)

        self.number_of_documents = 0
        self.vocabulary_size = 0

    def build_corpus(self):
        """
        Read document, fill in self.documents, a list of list of word
        self.documents = [["the", "day", "is", "nice", "the", ...], [], []...]
        
        Update self.number_of_documents
        """
        # Read file
        f = open('test.txt', encoding="utf8")
        lines = f.readlines()
        # Make each line in the file its own list inside the list
        self.documents = [line.strip().split() for line in lines]
        #update the number_of_documents var
        self.number_of_documents = len(self.documents)
        f.close()
        
    def build_vocabulary(self):
        """
        Construct a list of unique words in the whole corpus. Put it in self.vocabulary
        for example: ["rain", "the", ...]

        Update self.vocabulary_size
        """
        indptr = [0]
        indices = []
        data =[]
        vocabulary = {}
        
        for d in self.documents:
            for term in d:
                index = vocabulary.setdefault(term, len(vocabulary))
                indices.append(index)
                data.append(1)
            indptr.append(len(indices))
            
        self.vocabulary = list(vocabulary.keys())
        self.vocabulary_size = len(self.vocabulary)
        
        
    def build_term_doc_matrix(self):
        """
        Construct the term-document matrix where each row represents a document, 
        and each column represents a vocabulary term.

        self.term_doc_matrix[i][j] is the count of term j in document i
        """
        matrix = []
        for doc in self.documents:
            bag_vector = np.zeros(len(self.vocabulary))
            for w in doc:
                for i,word in enumerate(self.vocabulary):
                    if word == w:
                        bag_vector[i] += 1
                matrix.append(bag_vector)
        np.array(matrix)
        self.term_doc_matrix = matrix


    def initialize_randomly(self, number_of_topics):
        """
        Randomly initialize the matrices: document_topic_prob(pi) and topic_word_prob
        which hold the probability distributions for P(z | d) and P(w | z): self.document_topic_prob, and self.topic_word_prob

        Don't forget to normalize!
        HINT: you will find numpy’s random matrix useful [https://docs.scipy.org/doc/numpy-1.15.0/reference/generated/numpy.random.random.html]
        """
        dt = np.random.random_sample((self.number_of_documents, number_of_topics))
        for a in dt:
            row_sum = np.sum(a)
            a /= row_sum
        self.document_topic_prob = dt
        
        tw = np.random.random_sample((number_of_topics, self.vocabulary_size))
        for b in tw:
            row_sum = np.sum(b)
            b /= row_sum
        self.topic_word_prob = tw 


    def initialize_uniformly(self, number_of_topics):
        """
        Initializes the matrices: self.document_topic_prob and self.topic_word_prob with a uniform 
        probability distribution. This is used for testing purposes.

        DO NOT CHANGE THIS FUNCTION
        """
        self.document_topic_prob = np.ones((self.number_of_documents, number_of_topics))
        self.document_topic_prob = normalize(self.document_topic_prob)

        self.topic_word_prob = np.ones((number_of_topics, len(self.vocabulary)))
        self.topic_word_prob = normalize(self.topic_word_prob)

    def initialize(self, number_of_topics, random=False):
        """ Call the functions to initialize the matrices document_topic_prob and topic_word_prob
        """
        print("Initializing...")

        if random:
            self.initialize_randomly(number_of_topics)
        else:
            self.initialize_uniformly(number_of_topics)

    def expectation_step(self):
        """ The E-step updates P(z | w, d)
        """
        print("E step:")
        
        tp = []
        for x in self.document_topic_prob:
            for y in self.topic_word_prob:
                t0 = x[0] * y
                t1 = x[1] * y
                sums0 = np.sum(t0)
                sums1 = np.sum(t1)
                t0 /= sums0
                t1 /= sums1
                topics = np.stack((t0, t1))
            tp.append(topics)
        
        self.topic_prob = tp

    def maximization_step(self, number_of_topics):
        """ The M-step updates P(w | z)
        """
        print("M step:") 
        
        # update P(w | z) self.topic_word_prob
        r = 0
        mults0 = []
        for x in np.array(self.term_doc_matrix):
            mult0 = x * self.topic_prob[r][0]
            mults0.append(mult0)
            r += 1
            if r== 1000:
                break
        sums0 = np.sum(mults0, axis = 0)
        
        t = 0
        mults1 = []
        for q in self.term_doc_matrix:
            mult1 = q * self.topic_prob[t][1]
            mults1.append(mult1)
            t += 1
            if r==1000:
                break
        sums1 = np.sum(mults1, axis = 0)
        tw = [sums0, sums1]
            
        self.topic_word_prob = tw
            
        # update P(z | d) self.document_topic_prob
        dt = []
        multz = []
        e = 0
        for x in self.term_doc_matrix:
            f = 0
            mult1 = np.sum(x * self.topic_prob[e][f])
            f += 1
            mult2 = np.sum(x * self.topic_prob[e][f])
            e += 1
            multz = [mult1, mult2]
            dt.append(np.array(multz))
            if e == 1000:
                break
        for a in dt:
            row_sum = np.sum(a)
            a /= row_sum
            
        self.document_topic_prob = dt
        

    def calculate_likelihood(self, number_of_topics):
        """ Calculate the current log-likelihood of the model using
        the model's updated probability matrices
        
        Append the calculated log-likelihood to self.likelihoods

        """
        total0 = []
        total1 = []
        total = 0
        wordcounts = np.sum(self.term_doc_matrix, axis = 0)
        for i in self.document_topic_prob:
            top0 = i[0] * self.topic_word_prob[0] * wordcounts
            top1 = i[1] * self.topic_word_prob[1] * wordcounts
            total0.append(np.sum(top0))
            total1.append(np.sum(top1))
        total = sum(total0) + sum(total1)
        ll = math.log(total)

        self.likelihoods.append(ll)

    def plsa(self, number_of_topics, max_iterations, epsilon):

        """ 
        Model topics.
        """
        print ("EM iteration begins...")
        
        # build term-doc matrix
        self.build_term_doc_matrix()
        
        # Create the counter arrays.
        
        # P(z | d, w)
        self.topic_prob = np.zeros([self.number_of_documents, number_of_topics, self.vocabulary_size], dtype=np.float)

        # P(z | d) P(w | z)
        self.initialize(number_of_topics, random=True)

        # Run the EM algorithm
        current_likelihood = 0.0

        for iteration in range(max_iterations):
            print("Iteration #" + str(iteration + 1) + "...")

            self.expectation_step()
            self.maximization_step(number_of_topics)            
            self.calculate_likelihood(number_of_topics)
            print('Iteration likelihood: ', self.likelihoods[-1])


def main():
    documents_path = 'data\test.txt'
    corpus = Corpus(documents_path)  # instantiate corpus
    corpus.build_corpus()
    corpus.build_vocabulary()
    print(corpus.vocabulary)
    print("Vocabulary size:" + str(len(corpus.vocabulary)))
    print("Number of documents:" + str(len(corpus.documents)))
    number_of_topics = 2
    max_iterations = 50
    epsilon = 0.001
    corpus.plsa(number_of_topics, max_iterations, epsilon)
    print('Success!')


if __name__ == '__main__':
    main()